package com.example.projectfruit.customer

import android.text.Editable
import android.text.TextWatcher

open class CustomTextWatcher : TextWatcher {
    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        // Do nothing
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        // Do nothing
    }

    override fun afterTextChanged(p0: Editable?) {
        // Do nothing
    }
}
